package com.example.practise;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
  EditText etUsertName,password;
  Button button;
  TextView textView;
  ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
imageView=findViewById(R.id.img);
etUsertName=findViewById(R.id.etActedittext1);
password=findViewById(R.id.etActedittext2);
button=findViewById(R.id.btnSubmit);
textView=findViewById(R.id.tvActtextview);
        imageView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
            finish();
            }
        });
 button.setOnClickListener(new View.OnClickListener() {
     @Override
     public void onClick(View view) {
         String username=etUsertName.getText().toString();
         String pass=password.getText().toString();
         String temp="Your UserName and password is"+" "+username+"  "+pass;
         finish();
        Toast t=Toast.makeText(getApplicationContext(),temp,Toast.LENGTH_SHORT);
        t.show();
         Intent i = new Intent(MainActivity.this, calculator.class);
         startActivity(i);
     }
 });
    }
}